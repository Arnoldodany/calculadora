var express = require('express');
var router = express.Router();
const controller=require('../controllers/operations');
/* GET users listing. */
router.get('/results/:n1/:n2',controller.suma);
router.post('/results',controller.multi);
router.put('/results',controller.div);
router.patch('/results',controller.potencia);
router.delete('/results/:n1/:n2',controller.resta);
module.exports = router;
